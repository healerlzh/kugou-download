﻿using kugoudownload;
using System.Text.Json.Nodes;

//程序开始
Start:

//搜索分页
int PageIndex = 1;
//关键字
var searchKeyWord = string.Empty;
//操作类型
string operationKey = string.Empty;

var kugouClient = new KugouHttpUtil();


Console.WriteLine("输入关键词:");
searchKeyWord = Console.ReadLine()??"王心凌";

SearchNext:
var singerlist = kugouClient.Search(searchKeyWord, PageIndex);

Operation:

Console.WriteLine("\n请输入:全部下载(A)  下一页(N)  重新搜索(S) 关闭程序(E) 单曲下载请输入序号 \n");
string userInput= Console.ReadLine()??"1";

int writeIndex = 1;
if (userInput.Trim().ToUpper().Equals("A"))
{
    kugouClient.DownloadAll(singerlist);
    goto Operation;
}
else if (userInput.Trim().ToUpper().Equals("N"))
{
    PageIndex++;
    goto SearchNext;
}
else if (userInput.Trim().ToUpper().Equals("S"))
{
    goto Start;
}
else if (int.TryParse(userInput, out writeIndex))
{
    if (writeIndex > 0 && writeIndex <= singerlist.Count)
    {
        kugouClient.Download(singerlist[writeIndex - 1]);
    }
    goto Operation;
}

Console.WriteLine("操作完成.........");