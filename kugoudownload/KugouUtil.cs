﻿//using Furion.ClayObject;
//using Furion.DataEncryption;
//using Furion.RemoteRequest.Extensions;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace kugoudownload
//{
//    public class KugouUtil
//    {
//        Dictionary<string, object> headers = new Dictionary<string, object> {
//                { "User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36" }
//        };

//        /// <summary>
//        /// 搜索
//        /// </summary>
//        /// <param name="searchKeyWord">关键字</param>
//        /// <param name="PageIndex">分页</param>
//        /// <returns></returns>
//        public async Task<List<dynamic>> Search(string searchKeyWord, int PageIndex)
//        {
//            string t = (DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds.ToString();
            

//            string[] sign_params = {"NVPh5oo715z5DIWAeQlhMDsWXXQV4hwt", "bitrate=0", "callback=callback123",
//                       "clienttime=" + t, "clientver=2000", "dfid=-", "inputtype=0", "iscorrection=1",
//                       "isfuzzy=0",
//                       "keyword=" + searchKeyWord, "mid=" + t, "page=" + PageIndex, "pagesize=10",
//                       "platform=WebFilter", "privilege_filter=0", "srcappid=2919", "token=", "userid=0",
//                       "uuid=" + t, "NVPh5oo715z5DIWAeQlhMDsWXXQV4hwt" };
//            var sign_paramStr = string.Join("", sign_params);
//            var signature = MD5Encryption.Encrypt(sign_paramStr);

//            var response = await "https://complexsearch.kugou.com/v2/search/song"
//                .SetHeaders(headers)
//                .SetQueries(new Dictionary<string, object> {
//                {"callback","callback123"},
//                {"page",PageIndex},
//                {"keyword",searchKeyWord??""},
//                {"pagesize","10"},
//                {"bitrate","0"},
//                {"isfuzzy","0"},
//                {"inputtype","0"},
//                {"platform","WebFilter"},
//                {"userid","0"},
//                {"clientver","2000"},
//                {"iscorrection","1"},
//                {"privilege_filter","0"},
//                {"token",""},
//                {"srcappid","2919"},
//                {"clienttime",t},
//                {"mid",t},
//                {"uuid",t},
//                {"dfid","-"},
//                {"signature",signature}
//            }).GetAsStringAsync();

//            //返回值为JsonpCallback
//            var responseJson = response.Replace("callback123(", "").TrimEnd().TrimEnd(')');

//            var clay = Clay.Parse(responseJson);

//            List<dynamic> singerlist = new List<dynamic>();
//            Console.WriteLine("序号   FileName");

//            int index = 0;
//            foreach (var item in clay.data.lists)
//            {
//                index++;
//                singerlist.Add(item);
//                Console.WriteLine($"{index.ToString().PadLeft(2, '0')}  {item.FileName}");
//            }
//            return singerlist;
//        }


//        /// <summary>
//        /// 下载
//        /// </summary>
//        /// <param name="song_info"></param>
//        public async Task Download(dynamic song_info)
//        {
//            //获取文件下载路径
//            var responFileInfo = await "https://wwwapi.kugou.com/yy/index.php"
//                .SetHeaders(headers)
//                .SetQueries(new Dictionary<string, object>
//                {
//                    {"r", "play/getdata" },
//                    {"callback", "jQuery191035601158181920933_1653052693184" },
//                    {"hash", song_info.FileHash },
//                    {"dfid", "2mSZvv2GejpK2VDsgh0K7U0O" },
//                    {"appid", "1014" },
//                    {"mid", "c18aeb062e34929c6e90e3af8f7e2512" },
//                    {"platid", "4" },
//                    {"album_id", song_info.AlbumID },
//                    {"_", "1653050047389" }
//                }).GetAsStringAsync();

//            var responFileInfoJson = responFileInfo.Substring(42).TrimEnd().TrimEnd(';').TrimEnd(')');
//            var clay = Clay.Parse(responFileInfoJson);
//            string fileUrl = clay.data.play_url;

//            //下载文件
//            var bytes = await fileUrl.SetHeaders(headers).GetAsByteArrayAsync();

//            if (!Directory.Exists("./music"))
//            {
//                Directory.CreateDirectory("./music");
//            }
//            using (FileStream fs = new FileStream($"./music/{clay.data.audio_name}.mp3", FileMode.Create, FileAccess.Write))
//            {
//                fs.Write(bytes, 0, bytes.Length);
//                Console.WriteLine($"{fs.Name}");
//            }
//        }


//        public void DownloadAll(List<dynamic> list)
//        {
//            Task[] tasks= new Task[list.Count];

//            for (int i = 0; i < tasks.Count(); i++)
//            {
//                tasks[i] = Download(list[i]);
//                Thread.Sleep(300);
//            }
//            Task.WaitAll(tasks);
//        }
//    }
//}
